// Variable Define

var express = require('express');
var router  = express.Router();
var router1 = express.Router();
var storage = require('../services/storage.js');
var User =     require('../services/Models/user');
var mongoose   = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test'); // connect to our database


// Handle the connection event
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

// FUNCTION DEFINE
function init() {
  User.remove({} , function(err){
    if(err){
      console.log('error');
      return err;

    };
  });
  var examples = storage.getAll();
  for (i=0 ; i<examples.length ; i++){
    var user = new User();
    user.name = examples[i].name;
    user.pass = examples[i].pass;
    user.save(function(err) {
      if(err){
        console.log('error');
        return err;
      };
    });
  }
  console.log('Database Initialized')
}

// Start functions

db.once('open', function() {
  console.log("DB connection alive");
  init();
});





/* GET users listing. */
router.get('/', function(req, res, next) {
  // res.render('users', { users: storage.getAll() });
  User.find(function(err, users1) {
    if(err)
      res.send(err);
    console.log(users1);
    res.render('users', { users: users1 });
    // res.json(users);
  });

});

// autentication

router.route('/auth')
  .get(function(req, res){
    res.send('Got me')
  })
  .post(function(req, res){
    console.log('usuario')
    console.log(req.body)
    User.find({name : req.body.name}, function(err,user){
      console.log(user)
      if(user.pass != null){
        if(err)
          res.redirect('../')
        if(user[0].pass==req.body.pass){
          res.redirect('/users')
        }
      }
      else {
        res.redirect('../')
      }

    })


  })

// Sign In

router.route('/signin')
  .get(function(req, res){
    res.send('Sign In')
  })
  .post(function(req, res){
    var user = new User();
    user.name = req.body.name;
    user.pass = req.body.pass;

    user.save(function(err){
      if(err)
        res.send(err);
      res.json(user);
    })
  })


//
  router.route('/:id')
   .get(function(req, res, next) {
     console.log(req.params.id)
     User.findById(req.params.id, function(err, user1) {
       console.log(user1)
         if(err){
           console.log('ERROR')
           res.send(err);
         };
         // response = user;
         console.log('usuario' + user1);
         // res.json(user1);
         res.render('user', { user: user1 });
     });
   })

  .post( function(req,res) {
    User.findById(req.params.id, function(err, user1){
      console.log(user1)
        if(err){
          console.log('ERROR')
          res.send(err);
        };
        // response = user;
        console.log('usuario' + user1);
        // res.json(user1);
        user1.name=req.body.name;
        user1.pass=req.body.pass;

        user1.save(function(err) {
          if(err)
            res.send(err)
          console.log(user1)
          res.redirect('/users')
        })
    })

  })

router.get('/remove/:id' , function(req, res){
  User.remove({_id: req.params.id}, function(err,user) {
    if(err)
      res.send(err);
    res.redirect('/users')
  })


})




module.exports = router;
