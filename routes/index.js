var express = require('express');
var Users   = require('./users')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('login', { title: 'Express' });
});

router.post('/login', function(req, res, next) {
    res.redirect(307,'/users/auth');
});

router.route('/signin')
  .get(function(req, res , next) {
    res.render('signin', { title: 'Sign In' });
  })

  .post(function(req, res, next) {
      res.redirect(307,'/users/signin');
  });


module.exports = router;
